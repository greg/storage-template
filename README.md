# storage-test-template

Large repository files included:

- `dj-shadow.mp3` - 11.1MB
- `sd.zip` - 20.6MB
- `websecurity.pdf` - 61.9MB

Total repository storage size: 64MB
